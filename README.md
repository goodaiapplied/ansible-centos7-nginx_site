
Ansible role for a default nginx site
============

## Summary

This Ansible role is abstract, as in, to be used as a base for other dependencies.

 - Enables a certain nginx site, relying on the structure established by `ansible-centos7-nginx_base`

## Usage

You need to provide the `site` variable, in this case it's `habitable`

```yaml
dependencies:
  - { role: centos7/webserver/site, site: habitable }
```

On top of that, your role needs to provide the `templates/habitable.site.conf` serverblock configuration file.

A sensible default looks like this:

```nginx
server {
    server_name {{ site }}.com;

    error_page  500 502 503 504  /50x.html;
    location = /50x.html {
        root  /usr/share/nginx/html;
    }

    listen 80;
    listen [::]:80;

    root {{ nginx_root_dir }}/{{ site }}/;
    index index.html index.htm index.php;


    location / {
            try_files $uri $uri/ =404;
    }
}

```